import logging

import boto3
from botocore import UNSIGNED
from botocore.client import Config
import requests

DOMAIN = 'podgist.com'
API_CONTENT = 'api-content'
API_FORMAT_VERSION = 1
BASE_URL = f'https://www.{DOMAIN}'
CLIENT_ID = '39efhncar9cbi4akf0h66o2g85'
REGION_NAME = 'us-east-1'
SUBSCRIPTION_URL = 'https://70hkueewe5.execute-api.us-east-1.amazonaws.com/prod/user/%s/subscription'

logger = logging.getLogger(__name__)


class AccessDenied(Exception):
    """
    Represents denial of access to an authenticated user.
    """


class Post:
    """
    Wrapper around an audio transcription post, such as a podcast episode.
    """

    def __init__(self, **kwargs):
        self.source = None
        self.slug = None
        self.url = None
        self.raw_transcript_url = None
        self.lastmod = None
        self._transcript = None
        self.__dict__.update(kwargs)

    @property
    def transcript(self):
        """
        Retrieves the keyword transcript raw text.
        """
        if not self._transcript:
            text = self.source.api._get_transcript(self.source.slug, self.slug)
            self._transcript = text
        return self._transcript

    def __str__(self):
        return '<%s: %s/%s>' % (type(self).__name__, self.source.slug, self.slug)


class Source:
    """
    Wrapper around an audio transcription source, such as a podcast.
    """

    def __init__(self, **kwargs):
        self.api = None
        self.public_url = None
        self.api_url = None
        self.lastmod = None
        self.slug = None
        check_posts = kwargs.pop('check_posts', True)
        self.__dict__.update(kwargs)
        self._posts = None
        if check_posts:
            self._load_posts()

    def __len__(self):
        self._load_posts()
        return len(self._posts['results'])

    def __str__(self):
        return '<%s: %s>' % (type(self).__name__, self.slug)

    def _load_posts(self):
        """
        Downloads the post JSON and caches locally.
        """
        if self._posts:
            return
        url = f'{BASE_URL}/api/{API_FORMAT_VERSION}/{self.slug}.json'
        r = requests.get(url)
        assert r.status_code == 200
        self._posts = r.json()

    @property
    def posts(self):
        """
        Iterates over all posts available for this source.
        """
        self._load_posts()
        for data in self._posts['results']:
            data['source'] = self
            yield Post(**data)


class Podgist:
    """
    Top-level API wrapper.
    """

    def __init__(self):
        self.username = None
        self.password = None
        self.authToken = None
        self._source_slug_to_data = {}
        self._sources = None

    def login(self, username, password):
        """
        Authenticates the user to the Cognito user pool.
        """
        self.username = username
        self.password = password
        logger.info('Authenticating...')
        provider_client = boto3.client('cognito-idp', region_name=REGION_NAME, config=Config(signature_version=UNSIGNED))
        resp = provider_client.initiate_auth(
            AuthFlow='USER_PASSWORD_AUTH', AuthParameters={
                'USERNAME': self.username,
                'PASSWORD': self.password
            }, ClientId=CLIENT_ID
        )
        self.authToken = resp['AuthenticationResult']['IdToken']
        logger.info('Success.')

    @property
    def is_logged_in(self):
        """
        Returns true if the user is authenticated. Returns false otherwise.
        """
        return bool(self.authToken)

    def _get_signed_cookies_path(self, source_slug):
        """
        Retrieves the path to use when signing cookies.
        """
        return f'{API_CONTENT}/{source_slug}/*'

    def _get_signed_cookies_for_source(self, source_slug):
        """
        Retrieves signed cookies for accessing subscriber-only content for content source.
        """
        assert self.username, 'Must login first.'
        logger.info('Getting signed cookies...')
        url = SUBSCRIPTION_URL % (self.username,)
        requested_path = self._get_signed_cookies_path(source_slug)
        headers = {'Authorization': self.authToken}
        r = requests.get(url, headers=headers, params={'path': requested_path})
        assert r.status_code == 200
        self._source_slug_to_data[source_slug] = r.json()
        logger.info('Success.')

    def _get_transcript(self, source_slug, post_slug):
        """
        Retrieves the raw text-only keyword transcript.
        """
        assert self.is_logged_in, 'Must be logged in first.'
        if source_slug not in self._source_slug_to_data:
            self._get_signed_cookies_for_source(source_slug)
        logger.info('Retrieving transcript for source %s and post %s.', source_slug, post_slug)
        jar = requests.cookies.RequestsCookieJar()
        requested_path = self._get_signed_cookies_path(source_slug)
        accepted_path = '/%s' % requested_path[:-1] # Reformat path from "slug/*" to "/slug/"
        json_data = self._source_slug_to_data[source_slug]
        if not json_data['cookies']:
            raise AccessDenied('Your account does not include access to source "%s".' % source_slug)
        for k, v in json_data['cookies'].items():
            jar.set(k, v, domain=DOMAIN, path=accepted_path)
        url = f'{BASE_URL}/{API_CONTENT}/{source_slug}/{post_slug}/transcript.txt'
        logger.info('Retrieving %s.', url)
        r = requests.get(url, cookies=jar)
        assert r.status_code == 200, 'Unable to retrieve transcript: %s' % r.text
        return r.text

    def find_source(self, slug, **kwargs):
        """
        Loads a specific source with the given slug.
        """
        return Source(slug=slug, api=self, **kwargs)

    def find_post(self, source_slug, post_slug):
        """
        Loads a specific post with the given slugs.
        """
        source = self.find_source(source_slug, check_posts=False)
        post = Post(source=source, slug=post_slug)
        return post

    @property
    def sources(self):
        """
        Iterates over all available sources from the main JSON index.
        """
        if not self._sources:
            url = f'{BASE_URL}/api/{API_FORMAT_VERSION}/index.json'
            r = requests.get(url)
            assert r.status_code == 200
            self._sources = r.json()
        for data in self._sources['results']:
            data['api'] = self
            data['check_posts'] = False
            yield Source(**data)
