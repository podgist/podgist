#!/bin/bash
set -e

if [ ! -d .env ]; then
    ./init_virtualenv.sh
fi
. .env/bin/activate

echo "[$(date)] Building package."
python setup.py sdist

echo "[$(date)] Uploading to PyPI."
twine upload dist/*

echo "[$(date)] Done."
