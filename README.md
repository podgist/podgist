Podgist API Interface
=====================

This is a Python package for interacting with the subscriber API at [podgist.com](https://www.podgist.com).

Installation
------------

Install from PyPI with:

    pip install podgist

Usage
-----

    from podgist.api import Podgist

    # Initialize the base API wrapper class.
    api = Podgist()

    # Iterate over all published podcast sources.
    for source in api.sources:
        print('Source:', source.slug)

    # Load a specific source and find how many episodes have been transcribed.
    joe_rogan = api.find_source('joe-rogan-experience')
    print('Count:', len(joe_rogan))

    # Iterate over all posts within a source.
    for post in joe_rogan.posts:
        print('Post:', post.slug)

    # Authenticate to access subscriber-only content.
    api.login(username='myusername', password='mypassword')

    # Retrieve transcript.
    print('Transcript:', post.transcript)

    # Load a specific post by its source and post slug.
    post = api.find_post('joe-rogan-experience', 'jre-mma-show-86-with-josh-thomson')

    # Retrieve that post's transcript.
    print('Transcript:', post.transcript)
