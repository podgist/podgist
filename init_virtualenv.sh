#!/bin/bash
set -e

VENV=${1:-.env}

cd "$(dirname "$0")"

echo "[$(date)] Removing existing virtualenv if it exists."
[ -d $VENV ] && rm -Rf $VENV

echo "[$(date)] Creating virtual environment."
#python3.7 -m venv $VENV
virtualenv -p python3.7 $VENV

echo "[$(date)] Activating virtual environment."
. $VENV/bin/activate

echo "[$(date)] Upgrading pip."
pip install -U pip setuptools wheel

echo "[$(date)] Installing pip requirements."
pip install -r requirements.txt -r requirements-test.txt

echo "[$(date)] Done."
